#!/usr/bin/python2.7
# -*- coding:utf-8 -*-
from math import sin, cos, pi
import rospy
import tf
from motor_driver.msg import channel
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from nav_msgs.msg import Odometry
from threading import Thread

thread = Thread()

class Odom:
  def __init__(self):
    print("Odometry node started")
    rospy.init_node('Odometry')

    self.odom_pub = rospy.Publisher("odom", Odometry, queue_size=1)
    self.odom_broadcaster = tf.TransformBroadcaster()

    rospy.Subscriber("/front/spd", channel, self.f_spd_callback)
    rospy.Subscriber("/rear/spd", channel, self.r_spd_callback)

    # define encoder count value
    self.fl_spd = 0
    self.fr_spd = 0
    self.rl_spd = 0
    self.rr_spd = 0
   
   # define odom tf value
    self.x = 0.0
    self.y = 0.0
    self.th = 0.0
    self.vx = 0.1
    self.vy = -0.1
    self.vth = 0.1


    # Set up parameters
    try:
        self.agv_len = rospy.get_param("~agv_len")
        self.agv_width = rospy.get_param("~agv_width")
        self.wheel_rad = rospy.get_param("~wheel_rad")
        self.gear_ratio = rospy.get_param("~gear_ratio")
    except KeyError:
        print("cannot get param")
        self.agv_len = 0.85
        self.agv_width = 0.85
        self.wheel_rad = 0.2
        self.gear_ratio = 40.0

    self.scale = (2 * pi * self.wheel_rad) / (60 * self.gear_ratio)
  
  # define time
    self.current_time = rospy.Time.now()
    self.last_time = rospy.Time.now()
    self.rate = rospy.Rate(100)

     #node spin
    thread(target=self.main())


  def f_spd_callback(self, channel):
    self.fl_spd = channel.left
    self.fr_spd = channel.right
    self.current_time = channel.header.stamp

  def r_spd_callback(self, channel):
    self.rl_spd = channel.left
    self.rr_spd = channel.right

  def main(self):
    while not rospy.is_shutdown():

      self.vx = (self.fl_spd + self.fr_spd + self.rr_spd + self.rl_spd) * self.scale / 4
      self.vy = -(self.fl_spd - self.fr_spd + self.rr_spd - self.rl_spd) * self.scale / 4
      self.vth = (self.fl_spd - self.fr_spd - self.rr_spd + self.rl_spd) * self.scale/(self.agv_len + self.agv_width) / 2

      # self.current_time = rospy.Time.now()
 
      # compute odometry in a typical way given the velocities of the robot
      dt = (self.current_time - self.last_time).to_sec()
      delta_x = (self.vx * cos(self.th) - self.vy * sin(self.th)) * dt
      delta_y = (self.vx * sin(self.th) + self.vy * cos(self.th)) * dt
      delta_th = self.vth * dt

      self.x += delta_x
      self.y += delta_y
      self.th += delta_th

      # since all odometry is 6DOF we'll need a quaternion created from yaw
      odom_quat = tf.transformations.quaternion_from_euler(0, 0, self.th)

      # first, we'll publish the transform over tf
      self.odom_broadcaster.sendTransform(
          (self.x, self.y, 0.),
          odom_quat,
          self.current_time,
          "base_link",
          "odom"
      )

      # next, we'll publish the odometry message over ROS
      odom = Odometry()
      odom.header.stamp = self.current_time
      odom.header.frame_id = "odom"

      # set the position
      odom.pose.pose = Pose(Point(self.x, self.y, 0.), Quaternion(*odom_quat))

      # set the velocity
      odom.child_frame_id = "base_link"
      odom.twist.twist = Twist(Vector3(self.vx, self.vy, 0), Vector3(0, 0, self.vth))

      # publish the message
      self.odom_pub.publish(odom)

      self.last_time = self.current_time
      self.rate.sleep()

if __name__ == '__main__':
  Odom()
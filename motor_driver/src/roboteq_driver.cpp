#include "ros/time.h"
#include <motor_driver/channel.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <serial/serial.h>
#include <signal.h>
#include <sstream>
#include <string>

void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

class MainNode
{

public:
  MainNode();

public:
  //
  // cmd_vel subscriber
  //
  void cmdvel_callback(const motor_driver::channel& command);
  void driver_configure();

  void data_query();
  void data_parsing();
  void timer_callback(const ros::TimerEvent& e);

  void encoder_publish();
  void speed_publish();

  int run();

protected:
  ros::NodeHandle nh;

  serial::Serial controller;

  static int32_t to_rpm(int32_t x) { return int32_t(x); }

  //
  // cmd subscriber
  //
  ros::Subscriber cmdvel_sub;

  //
  // speed publisher
  //
  ros::Publisher encoder_data;
  ros::Publisher speed_data;
  ros::Timer sampling_time;

  // buffer for reading encoder counts
  int data_idx;
  char data_buf[24];

  // toss out initial encoder readings
  char data_encoder_toss;

  int32_t data_encoder_left;
  int32_t data_encoder_right;
  int32_t encoder_rpm_left;
  int32_t encoder_rpm_right;

  // settings

  std::string cmdvel_topic;
  std::string port;
  int baud;
  bool open_loop;
  int encoder_ppr;
  int encoder_cpr;
  double max_rpm = 2000;
  int index = 0;

  float motor_right_speed = 0;
  float motor_left_speed = 0;
  int32_t motor_mode = -1;
};

MainNode::MainNode()
    : data_idx(0), data_encoder_toss(5), data_encoder_left(0),
      data_encoder_right(0), open_loop(false), baud(115200), encoder_ppr(0),
      encoder_cpr(0), index(0)
// max_rpm(3000)
{

  // CBA Read local params (from launch file)
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("cmdvel_topic", cmdvel_topic, "cmd");
  nhLocal.param<std::string>("port", port, "/dev/ttyUSB0");
  nhLocal.param("baud", baud, 115200);
  nhLocal.param("open_loop", open_loop, false);
  nhLocal.param("encoder_ppr", encoder_ppr, 3000);
  nhLocal.param("encoder_cpr", encoder_cpr, 12000);
  nhLocal.param("index", index, 0);

  cmdvel_sub =
      nh.subscribe(cmdvel_topic, 1000, &MainNode::cmdvel_callback, this);
  encoder_data =
      nh.advertise<motor_driver::channel>("/enc", 10);
  speed_data =
      nh.advertise<motor_driver::channel>("/spd", 10);
  sampling_time =
      nh.createTimer(ros::Duration(0.01), &MainNode::timer_callback, this);
}

//
// cmd_vel subscriber
//

void MainNode::cmdvel_callback(const motor_driver::channel& command)
{

    motor_right_speed = command.right / max_rpm * 1000;
    motor_left_speed = command.left / max_rpm * 1000;
}

void MainNode::timer_callback(const ros::TimerEvent& e)
{

  if (motor_mode == 1)
  {
    if (open_loop)
    {
      // motor power (scale 0-1000)

      int32_t right_power = static_cast<int>(motor_right_speed * 1);
      int32_t left_power = static_cast<int>(motor_left_speed * 1);

      std::stringstream right_cmd;
      std::stringstream left_cmd;

      right_cmd << "!G 1 " << right_power << "\r";
      left_cmd << "!G 2 " << left_power << "\r";

      controller.write(right_cmd.str());
      controller.write(left_cmd.str());
      controller.flush();
    }
    else
    {
      // motor speed (rpm)

      int32_t right_rpm = static_cast<int>(motor_right_speed * 1);
      int32_t left_rpm = static_cast<int>(motor_left_speed * 1);

      std::stringstream right_cmd;
      std::stringstream left_cmd;

      right_cmd << "!S 1 " << right_rpm << "\r";
      left_cmd << "!S 2 " << left_rpm << "\r";

      controller.write(right_cmd.str());
      controller.write(left_cmd.str());
      controller.flush();
    }
  }
  else
  {
    int32_t right_rpm = 0;
    int32_t left_rpm = 0;

    std::stringstream right_cmd;
    std::stringstream left_cmd;

    right_cmd << "!S 1 " << right_rpm << "\r";
    left_cmd << "!S 2 " << left_rpm << "\r";

    controller.write(right_cmd.str());
    controller.write(left_cmd.str());
    controller.flush();
  }
  // speed_publish();
  // encoder_publish();
}

void MainNode::driver_configure()
{

  // stop motors
  controller.write("!G 1 0\r");
  controller.write("!G 2 0\r");
  controller.write("!S 1 0\r");
  controller.write("!S 2 0\r");
  controller.flush();

  // disable echo
  controller.write("^ECHOF 1\r");
  controller.flush();

  // enable watchdog timer (1000 ms)
  controller.write("^RWD 250\r");
  //  controller.write("^RWD 250\r");

  // set motor operating mode (1 for closed-loop speed)
  if (open_loop)
  {
    controller.write("^MMOD 1 0\r");
    controller.write("^MMOD 2 0\r");
  }
  else
  {
    controller.write("^MMOD 1 1\r");
    controller.write("^MMOD 2 1\r");
  }

  // set motor amps limit (20 A * 10)
  controller.write("^ALIM 1 2000\r");
  controller.write("^ALIM 2 2000\r");

  // set max speed (rpm) for relative speed commands
  //  controller.write("^MXRPM 1 82\r");
  //  controller.write("^MXRPM 2 82\r");
  controller.write("^MXRPM 1 2000\r");
  controller.write("^MXRPM 2 2000\r");

  // set max acceleration rate (500 rpm/s * 10)
  //  controller.write("^MAC 1 2000\r");
  //  controller.write("^MAC 2 2000\r");
  controller.write("^MAC 1 5000\r");
  controller.write("^MAC 2 5000\r");

  // set max deceleration rate (2000 rpm/s * 10)
  controller.write("^MDEC 1 20000\r");
  controller.write("^MDEC 2 20000\r");

  // set number of poles
  controller.write("^BPOL 1 2\r");
  controller.write("^BPOL 2 2\r");

  // set PID parameters (gain * 10)
  controller.write("^KP 1 0\r");
  controller.write("^KP 2 0\r");
  controller.write("^KI 1 50\r");
  controller.write("^KI 2 50\r");
  controller.write("^KD 1 0\r");
  controller.write("^KD 2 0\r");

  // set encoder mode (18 for feedback on motor1, 34 for feedback on motor2)
  controller.write("^EMOD 1 18\r");
  controller.write("^EMOD 2 34\r");

  // set encoder counts (ppr)
  std::stringstream right_enccmd;
  std::stringstream left_enccmd;
  right_enccmd << "^EPPR 1 " << encoder_ppr << "\r";
  left_enccmd << "^EPPR 2 " << encoder_ppr << "\r";
  controller.write(right_enccmd.str());
  controller.write(left_enccmd.str());
  controller.flush();
}

void MainNode::data_query()
{
  // controller.write("# C_?CR_?BA_?V_?S_# 10\r");
  controller.write("# C_?C_?S_# 10\r");
  controller.flush();
}

void MainNode::encoder_publish()
{
  motor_driver::channel encoder_value;
  encoder_value.header.stamp = ros::Time::now();
  encoder_value.left = data_encoder_left;
  encoder_value.right = data_encoder_right;

  encoder_data.publish(encoder_value);
}

void MainNode::speed_publish()
{
  motor_driver::channel speed_value;
  speed_value.header.stamp = ros::Time::now();
  speed_value.left = encoder_rpm_left;
  speed_value.right = encoder_rpm_right;

  speed_data.publish(speed_value);
}

void MainNode::data_parsing()
{

  // read sensor data stream from motor controller
  if (controller.available())
  {
    char ch = 0;
    if (controller.read((uint8_t*)&ch, 1) == 0)
      return;
    if (ch == '\r')
    {
      data_buf[data_idx] = 0;

      // Absolute encoder value C

      if (data_buf[0] == 'C' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_encoder_toss > 0)
          {
            --data_encoder_toss;
            break;
          }
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            data_encoder_right = (int32_t)strtol(data_buf + 2, NULL, 10);
            data_encoder_left = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            // printf("encoder right: %d left: %d \n
            // ",data_encoder_right,data_encoder_left);
            encoder_publish();
            break;
          }
        }
      }
      // S= is speed in RPM
      if (data_buf[0] == 'S' && data_buf[1] == '=')
      {
        int delim;
        for (delim = 2; delim < data_idx; delim++)
        {
          if (data_buf[delim] == ':')
          {
            data_buf[delim] = 0;
            encoder_rpm_right = (int32_t)strtol(data_buf + 2, NULL, 10);
            encoder_rpm_left = (int32_t)strtol(data_buf + delim + 1, NULL, 10);
            // printf("delim: %d encoder right: %d left: %d \n
            // ",delim,encoder_rpm_right,encoder_rpm_left);
            speed_publish();
            break;
          }
        }
      }

      data_idx = 0;
    }
    else if (data_idx < (sizeof(data_buf) - 1))
    {
      data_buf[data_idx++] = ch;
    }
  }
}

int MainNode::run()
{

  serial::Timeout timeout = serial::Timeout::simpleTimeout(1000);
  controller.setPort(port);
  controller.setBaudrate(baud);
  controller.setTimeout(timeout);

  // TODO: support automatic re-opening of port after disconnection
  while (ros::ok())
  {
    // ROS_INFO_STREAM("Opening serial port on " << port << " at " << baud <<
    // "..." );
    try
    {
      controller.open();
      if (controller.isOpen())
      {
        // ROS_INFO("Successfully opened serial port");
        break;
      }
    }
    catch (serial::IOException e)
    {
      ROS_WARN_STREAM("serial::IOException: " << e.what());
    }
    ROS_WARN("Failed to open serial port");
    sleep(5);
  }

  driver_configure();
  data_query();


  while (ros::ok())
  {

    data_parsing();

    ros::spinOnce();

  }

  if (controller.isOpen())
    controller.close();

  ROS_INFO("Exiting");

  return 0;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "main_node");

  MainNode node;

  signal(SIGINT, mySigintHandler);

  return node.run();
}

#!/usr/bin/python3.8
# -*- coding:utf-8 -*-

# this node for convert value (idas raw_data => linear velocity)
from os import ST_WRITE
import rospy
import signal
import sys
import math
from idas_driver.msg import idas
from joy_teleop.msg import velocity 
from threading import Thread 
from enum import Enum

thread = Thread()
class Mode(Enum):
  AUTO = 0
  MANUAL = 1
  EMC = 2

class Teleop:
  def __init__(self):
    print(f"teleop node start")
    rospy.init_node("TELEOP")
    signal.signal(signal.SIGINT, self.exit_function)
    signal.signal(signal.SIGTERM, self.exit_function)
    #-----------------------------------------------------------------+
    # Publisher                                                       |
    #-----------------------------------------------------------------+
    self.velocity_pub = rospy.Publisher('/teleop', velocity, queue_size = 1)
    #-----------------------------------------------------------------+
    # Subscriber                                                      |
    #-----------------------------------------------------------------+
    rospy.Subscriber("/idas_driver", idas, self.rawIdas_callback)
    #-----------------------------------------------------------------+
    # gloabal variable                                                |
    #-----------------------------------------------------------------+
    self.rate = rospy.Rate(100)
    self.joy_noise = 0.1
    self.fast_mode = 1.5
    self.slow_mode = 0.5
    self.speed_scale = 1.11 #(60/54)
    self.x_linear = 0
    self.y_linear = 0
    self.th_angular = 0
    self.velth = 0.0
    self.vely = 0.0
    self.velx = 0.0
    self.an5 = 0.0
    self.ky4 = 0.0
    self.ky1 = 0.0
    self.connection = 0
    self.da3 = 0.0
    self.da4 = 0.0
    self.mode = Mode.AUTO
    self.scale = 1.0
    #-----------------------------------------------------------------+
    # etc                                                             |
    #-----------------------------------------------------------------+
    # self.main()
    thread(target=self.main())
    #-----------------------------------------------------------------+
  def exit_function(self):
    rospy.shutdown()
    sys.exit()

  def rawIdas_callback(self, data):
    # signal for linear(foward or backward)
    self.velx = data.AN3
    # signal for linear(left or right)
    self.vely = data.AN4
    # signal for theta(turn left or rignt)
    self.velth = data.AN2
    # signal for linear(convayor forward or backward)
    self.an5 = data.AN5
    # digital toggle button data
    self.ky4 = data.KY4
    # emc button
    self.ky1 = data.KY1
    # hex(0x40) dec(64) => connected
    self.connection = data.connection
    
    # idas_joystick device infomation
    self.da3 = data.DA3
    self.da4 = data.DA4

  def convert_speed(self):
    # x direction
    if self.velx >= self.joy_noise or self.velx <= -self.joy_noise:
      self.x_linear  = (self.velx - self.joy_noise * math.copysign(1,self.velx)) * self.speed_scale
    else:
      self.x_linear = 0
    
    # y direction
    if self.vely >= self.joy_noise or self.vely <= -self.joy_noise:
      self.y_linear   = (self.vely - self.joy_noise * math.copysign(1,self.vely)) * self.speed_scale
    else:
      self.y_linear = 0

    # z direction 
    if self.velth >= self.joy_noise or self.velth <= -self.joy_noise:
      self.th_angular = (self.velth - self.joy_noise * math.copysign(1,self.velth)) * self.speed_scale
    else:
      self.th_angular = 0
    
    #convayor
    if self.an5 >=self.joy_noise or self.an5 <= -self.joy_noise:
      self.convayor   = (self.an5 - self.joy_noise * math.copysign(1,self.an5)) * self.speed_scale  # acutally convayor control type is 0 or 1
    else:
      self.convayor = 0

    return self.x_linear, self.y_linear, self.th_angular

  def cal_vel(self, x):
    if x >= self.joy_noise or x <= -self.joy_noise:
      out  = (x - self.joy_noise * math.copysign(1,x)) * self.speed_scale
    else:
      out = 0
    return out

  def main(self):
    while not rospy.is_shutdown():
      joy_cmd = velocity()

      if self.connection == 1:
        if self.ky1 == 128:  # emergency
          print(f"emc mode ") 
          self.mode = Mode.EMC
        elif self.ky1 == 66:
          # when idas joystick connect & data received is correct
          if self.da3 == 56 and self.da4 == 2:
             # based on ky4 to decide slow_mode (21), fast mode (22), normal mode(20)             
            if self.ky4 == 22:
              self.mode = Mode.MANUAL
              # print(f"fast mode")
              self.scale = self.fast_mode
            elif self.ky4 == 21:
              self.mode = Mode.MANUAL
              # print(f"slow_mode")
              self.scale = self.slow_mode
            elif self.ky4 == 20:
              self.mode = Mode.MANUAL
              # print(f"normal mode ")
              self.scale = 1.0
            else:
              self.mode = Mode.AUTO
              print(f"auto mode ")
          else:
            self.mode = Mode.AUTO
            print(f"auto mode ")
        else: 
          self.mode = Mode.AUTO
          print(f"auto mode ")
      else:
        self.mode = Mode.AUTO
        print(f"auto mode idas disconnected")

      if self.mode == Mode.EMC:
        joy_cmd.mode = 1
        joy_cmd.velx  = 0
        joy_cmd.vely  = 0
        joy_cmd.angth = 0
      elif self.mode == Mode.MANUAL:
        joy_cmd.mode = 1
        joy_cmd.velx  = self.cal_vel(self.velx) * self.scale
        joy_cmd.vely  = -self.cal_vel(self.vely) * self.scale
        joy_cmd.angth = -self.cal_vel(self.velth) * self.scale
      else:
        joy_cmd.mode = 0
        joy_cmd.velx  = 0
        joy_cmd.vely  = 0
        joy_cmd.angth = 0
        
      self.velocity_pub.publish(joy_cmd)
      # print(f"mode:{joy_cmd.mode}, x:{joy_cmd.velx}, y:{joy_cmd.vely}, z:{joy_cmd.angth}")
      self.rate.sleep()

if __name__ == '__main__':
  Teleop()
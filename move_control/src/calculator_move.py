#!/usr/bin/python3.8
# -*- coding:utf-8 -*-
import math

class Move_calculator:
  def __init__(self):
    # 단위 (meter)
    self.AGV_length = 0.85
    self.AGV_width = 0.85
    self.wheel_radian = 0.2
    
    # 단위 (ratio)
    self.gear_ratio = 40
    self.scale_x = 1.0
    self.scale_y = 1.0
    self.scale_th = 1.0

    # slow_scale => speed diviend whth slow scale
    self.slow_scale = 10

    # 단위 (boolean)
    self.emergency = False
    self.emergency_speed = False

  def calculate(self, linear_x, linear_y, angular_th, emergency_speed, emergency):
    # 휠 지름 (radian)
    self.wheel_diameter = 2 * math.pi * self.wheel_radian

    linear_x = linear_x * self.scale_x * 120
    linear_y = linear_y * self.scale_y * 120
    angular_th = angular_th * self.scale_th * 120
    
    if (emergency_speed is True):
      self.front_left = (1 / self.wheel_diameter) * ((linear_x - linear_y - ((self.AGV_length + self.AGV_width)/2) * angular_th) * self.gear_ratio) / self.slow_scale
      self.front_right = (1 / self.wheel_diameter) * (linear_x + linear_y + ((self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio) / self.slow_scale
      self.rear_left = (1 / self.wheel_diameter) * (linear_x + linear_y - ((self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio) / self.slow_scale
      self.rear_right = (1 / self.wheel_diameter) * (linear_x - linear_y + ((self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio) / self.slow_scale
      
    elif (emergency is True):
      self.front_left = 0
      self.front_right = 0
      self.rear_left = 0
      self.rear_right = 0
      
    else:
      self.front_left = (1 / self.wheel_diameter) * (linear_x - linear_y - (self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio
      self.front_right = (1 / self.wheel_diameter) * (linear_x + linear_y + (self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio
      self.rear_left = (1 / self.wheel_diameter) * (linear_x + linear_y - (self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio
      self.rear_right = (1 / self.wheel_diameter) * (linear_x - linear_y + (self.AGV_length + self.AGV_width)/2 * angular_th) * self.gear_ratio
      
      # return list type
      return self.front_left, self.front_right, self.rear_left, self.rear_right

if __name__ == '__main__':
  Move_calculator()


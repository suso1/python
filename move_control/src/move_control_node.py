#!/usr/bin/env python3.8
import rospy
# from safety.msg import emc
from motor_driver.msg import channel
from std_msgs.msg import UInt8
from joy_teleop.msg import velocity
import signal
import sys
import math
from threading import Thread


thread = Thread()

# list number [shape] => vel[vx ,vy, va]
vx = 0
vy = 1
va = 2

class Move_control:
  def __init__(self):
    print("MOVE_CONTROL node start")
    rospy.init_node("move_control")
    #------------------------------------------------------------------------+
    # Publisher                                                              |
    #------------------------------------------------------------------------+
    self.pub_front_cmd = rospy.Publisher("/front/cmd", channel, queue_size=1)
    self.pub_rear_cmd = rospy.Publisher("/rear/cmd", channel, queue_size=1)
    #------------------------------------------------------------------------+
    # Subscriber                                                             |
    #------------------------------------------------------------------------+
    rospy.Subscriber("/emc_stop", UInt8, self.emc_callback)
    rospy.Subscriber("/emc_speed", UInt8, self.emcSpeed_callback)
    rospy.Subscriber("/teleop",velocity,self.joy_callback)
    rospy.Subscriber("/auto_cmd",velocity,self.auto_cmd_callback)
    #------------------------------------------------------------------------+
    # global variable                                                        |
    #------------------------------------------------------------------------+
    self.emc = 1
    self.emc_speed = 0
    self.emc_scale = 5
    self.front_left  = 0
    self.front_right = 0
    self.rear_left   = 0
    self.rear_right  = 0

    self.auto = [0,0,0]
    self.joy = [0,0,0]
    self.mode = 1
    self.mode_auto = 0
    self.scale = 5.0
    #------------------------------------------------------------------------+
    # etc                                                                    |
    #------------------------------------------------------------------------+
    signal.signal(signal.SIGINT, self.exit_function)
    signal.signal(signal.SIGTERM, self.exit_function)
    #------------------------------------------------------------------------+

    # Set up parameters
    try:
        self.agv_len = rospy.get_param("~agv_len")
        self.agv_width = rospy.get_param("~agv_width")
        self.wheel_rad = rospy.get_param("~wheel_rad")
        self.gear_ratio = rospy.get_param("~gear_ratio")
    except KeyError:
        print("cannot get param")
        self.agv_len = 0.85
        self.agv_width = 0.85
        self.wheel_rad = 0.2
        self.gear_ratio = 40.0

    self.rate = rospy.Rate(100)
    # self.main()
    thread(target=self.main())

  def exit_function(self):
    rospy.shutdown()
    sys.exit()

  def joy_callback(self,data):
    self.joy[vx] = data.velx
    self.joy[vy] = data.vely
    self.joy[va] = data.angth
    self.mode = data.mode
    # print(f"mode:{self.mode}, x:{self.joy[vx]}, y:{self.joy[vy]}, z:{self.joy[va]}")

  def emc_callback(self, data):
    # ?
    #self.emc = 1 - data.data
    self.emc = data.data

  def emcSpeed_callback(self, data):
    self.emc_speed = int(data.data)

  def auto_cmd_callback(self,data):
    self.auto[vx] = data.velx
    self.auto[vy] = data.vely
    self.auto[va] = data.angth
    self.mode_auto = data.mode

  def calculate_spd(self):
    # select manual mode or autometic mode
    vx_cmd = self.mode * self.joy[vx] + (1-self.mode) * self.mode_auto * self.auto[vx]
    vy_cmd = self.mode * self.joy[vy] + (1-self.mode) * self.mode_auto * self.auto[vy]
    va_cmd = self.mode * self.joy[va] + (1-self.mode) * self.mode_auto * self.auto[va]
    # print(f"mode:{self.mode}, x:{vx_cmd}, y:{vy_cmd}, z:{va_cmd}")

    # mecanum wheel direction calcualte
    self.front_left = self.emc * (1 / self.wheel_rad) * ((vx_cmd + vy_cmd + ((self.agv_len + self.agv_width)/2) * va_cmd) * self.gear_ratio)*self.scale
    self.front_right = self.emc * (1 / self.wheel_rad) * ((vx_cmd - vy_cmd - ((self.agv_len + self.agv_width)/2) * va_cmd) * self.gear_ratio)*self.scale
    self.rear_left = self.emc * (1 / self.wheel_rad) * ((vx_cmd - vy_cmd + ((self.agv_len + self.agv_width)/2) * va_cmd) * self.gear_ratio)*self.scale
    self.rear_right = self.emc * (1 / self.wheel_rad) * ((vx_cmd + vy_cmd - ((self.agv_len + self.agv_width)/2) * va_cmd) * self.gear_ratio)*self.scale
    # print(f"fl:{self.front_left}, fr:{self.front_right},rl:{self.rear_left},rr:{self.rear_right}")

  def publish(self):
    front_msgs = channel()
    rear_msgs = channel()

    #send motor vel command, when emc on then send command vel to 0
    if self.emc is 0:
      front_msgs.left  = 0
      front_msgs.right = 0
      rear_msgs.left   = 0
      rear_msgs.right  = 0

    elif int(self.emc_speed) is 1:
      front_msgs.left  = int(self.front_left  / self.emc_scale)
      front_msgs.right = int(self.front_right / self.emc_scale)
      rear_msgs.left   = int(self.rear_left   / self.emc_scale)
      rear_msgs.right  = int(self.rear_right  / self.emc_scale)

    else:
      front_msgs.left  = int(self.front_left)
      front_msgs.right = int(self.front_right)
      rear_msgs.left   = int(self.rear_left)
      rear_msgs.right  = int(self.rear_right)

    self.pub_front_cmd.publish(front_msgs)
    self.pub_rear_cmd.publish(rear_msgs)

  def main(self):
    while not rospy.is_shutdown():
      self.calculate_spd()
      self.publish()
      self.rate.sleep()

if __name__ == '__main__':
  Move_control()
#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-
import queue
import rospy
from idas_driver.msg import idas
from serial import Serial
import threading
import time
import sys
import signal

# thread function
thread = threading.Thread()
# threading lock
lock = threading.Lock()
ser = Serial()

# 쓰레드 종료 변수
exitThread = False

class Parsing():
  def __init__(self):
    print("idas 노드가 시작 되었습니다..")

    rospy.init_node('idas_driver', anonymous=True)
    self.pub = rospy.Publisher('/idas_driver',idas, queue_size=1)

    # 타겟 시리얼
    try:
      self.port = rospy.get_param("~port")
      self.baudrate = rospy.get_param("~baudrate")
    except Exception as e:
      print(f"cannot get idas port parameter")
      self.port = "/dev/ttyUSB1"
      self.baudrate = 115200

    # 시리얼 값을 라인 단위로 가져올 리스트 변수
    

    # 메인 함수 실행
    self.main()

  def connect(self, port, baudrate):
    ser.port = port
    ser.baudrate = baudrate

    if ser.is_open:
      # serial port is already connected
      print(f"시리얼 포트[{ser.port}]가 이미 열려있습니다.\n")
      try:
        # serial port will close
        print(f"시리얼 포트 닫는 중 ...\n")
        ser.close()
        # serial port is correctly closed
        print(f"시리얼 포트가 정상적으로 닫혔습니다. \n\n")

        # trying open serial port 
        print(f"시리얼 포트 여는중...\n")
        ser.open()
        # serial port is correctly opened
        print(f"시리얼 포트가 정상적으로 열렸습니다.")

      except Exception as e:
        # there are problem when close port
        print(f"시리얼 포트 닫는중에 문제가 생겼습니다.\n")
        # check below message
        print(f"아래를 참고하세요 \n")
        print(f"에러 메시지: {e}")
    
    else:
      try:
        print(f"시리얼 포트[{ser.port}] 여는중...\n")
        ser.close()
        ser.open()
        print(f"시리얼 포트가 정상적으로 열렸습니다.")
      except Exception as e:
        print(f"시리얼 포트 여는 중에 문제가 생겼습니다.\n")
        print(f"아래를 참고하세요 \n")
        print(f"에러 메시지: {e}")

  def read_value(self):
    self.value = None
    while not rospy.is_shutdown():
      if ser.readable():
        try:
          value = ser.readline()
          time.sleep(0.01)
          self.parse(data = str(value))
        
        except Exception as e:
          print(f"시리얼 값을 읽는 중 문제가 생겼습니다.")
          print(f"아래를 참고하세요 \n")
          print(f"에러 메시지: {e}")
    ser.close()
  
  def parse(self, data):
    idas_msg = idas()
    raw_data = data
    raw_data = raw_data[1:53].split(",")
    
    if "'#" in raw_data[0]:
        an1 = 1.0 - int(str(raw_data[0].strip("'#")),16) / 127.0
        an2 = 1.0 - int(str(raw_data[1]),16) / 127.0
        an3 = 1.0 - int(str(raw_data[2]),16) / 127.0
        an4 = 1.0 - int(str(raw_data[3]),16) / 127.0
        an5 = 1.0 - int(str(raw_data[4]),16) / 127.0
        an6 = 1.0 - int(str(raw_data[5]),16) / 127.0
        an7 = 1.0 - int(str(raw_data[6]),16) / 127.0
        an7 = 1.0 - int(str(raw_data[7]),16) / 127.0
      
        # idas joystick AN MSG 
        idas_msg.AN1 = an1
        idas_msg.AN2 = an2
        idas_msg.AN3 = an3
        idas_msg.AN4 = an4
        idas_msg.AN5 = an5
        idas_msg.AN6 = an6
        idas_msg.AN7 = an7

        # 디지털 토글 스위치
        ky1 = int(str(raw_data[8]),16)
        ky2 = int(str(raw_data[9]),16)
        ky3 = int(str(raw_data[10]),16)
        ky4 = int(str(raw_data[11]),16)

        idas_msg.KY1 = ky1
        idas_msg.KY2 = ky2
        idas_msg.KY3 = ky3
        idas_msg.KY4 = ky4

        # 상태 정보
        da1 = int(str(raw_data[12]),16)
        da2 = int(str(raw_data[13]),16)
        da3 = int(str(raw_data[14]),16)
        da4 = int(str(raw_data[15]),16)

        if (da2 == 64):
          bool_da2 = 1
        else:
          bool_da2 = 0

        idas_msg.DA1 = da1
        idas_msg.connection = bool_da2
        idas_msg.DA3 = da3
        idas_msg.DA4 = da4

        # 메시지 퍼블리쉬
        self.pub.publish(idas_msg)

  def signal_callback(self):
    sys.exit(0)

  def main(self):
    self.connect(self.port, self.baudrate)
    thread(target=self.read_value())
    #thread(target=self.parse(), arg=(self.value))
    signal.signal(signal.SIGINT, self.signal_callback)
if __name__ == "__main__":
  Parsing()

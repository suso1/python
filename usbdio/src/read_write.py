#!/usr/bin/python3.8
# -*- coding:utf-8 -*-
import rospy
from threading import Thread
from usbdio_driver import USBDIO
from usbdio.msg import io_in
from usbdio.msg import io_out
import sys
import signal

thread = Thread()
usbdio = USBDIO()

class USBDIO():
  def __init__(self):
    print("USBDIO node start")
    rospy.init_node("USBDIO")
    self.pub = rospy.Publisher("/io_in", io_in, queue_size=1)
    rospy.Subscriber("/io_out", io_out, self.write_continue)
    self.rate = rospy.Rate(100)
    self.main()

  def read_continue(self):
    self.io_in = io_in()
    while not rospy.is_shutdown():
        # read physical emergency button value
        self.io_in.emc_btn =  usbdio.in_signal(0,1)
        # print(self.io_in.emc_btn)
        self.pub.publish(self.io_in)
        self.rate.sleep()

  def write_continue(self, data):
    self.io_out = io_out()

    # red lamp on/off
    usbdio.out_signal(0 ,0, data.redL)
    # orange lamp on/off
    usbdio.out_signal(0 ,1, data.orangeL)
    # green lamp on/off
    usbdio.out_signal(0 ,2, data.greenL)
    # buzzer on/off
    usbdio.out_signal(0 ,3, data.buzzer)
    # power relay on/off
    usbdio.out_signal(0 ,4, data.pw_relay)
    # convayor on/off
    usbdio.out_signal(0 ,5, data.convayor)
    # change convayor direction
    usbdio.out_signal(0 ,6, data.convayor_dir)

  def main(self):
    thread(target = self.read_continue())

if __name__ == '__main__':
    USBDIO()
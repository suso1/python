#!/usr/bin/python3.8
# -*- coding:utf-8 -*-

from Automation.BDaq import *
from Automation.BDaq.InstantDoCtrl import InstantDoCtrl
from Automation.BDaq.InstantDiCtrl import InstantDiCtrl
from Automation.BDaq.BDaqApi import AdxEnumToString, BioFailed
import os

deviceDescription = "USB-4750,BID#0"
profilePath = os.path.dirname("./profile/DemoDevice.xml")

class USBDIO:
    # output signal handling
    def out_signal(self, port, gate, value):
        ret = ErrorCode.Success

        instantDoCtrl = InstantDoCtrl(deviceDescription)
        for _ in range(1):
            instantDoCtrl.loadProfile = profilePath

            # Write DO ports
            ret = instantDoCtrl.writeBit(port, gate, value)
            if BioFailed(ret):
                break

        # Close device and release any allocated resource.
        instantDoCtrl.dispose()

        # If something wrong in this execution, print the error code on screen for tracking.
        if BioFailed(ret):
            enumStr = AdxEnumToString("ErrorCode", ret.value, 256)
            print("Some error occurred. And the last error code is %#x. [%s]" % (ret.value, enumStr))
    
        return 0

    # input signal handling
    def in_signal(self, port, bit):
        ret = ErrorCode.Success

        instantDiCtrl = InstantDiCtrl(deviceDescription)

        for _ in range(1):
            instantDiCtrl.loadProfile = profilePath

        ret, data = instantDiCtrl.readAny(port, bit)

        # Close device and release any allocated resource
        instantDiCtrl.dispose()

        if BioFailed(ret):
            enumStr = AdxEnumToString("ErrorCode", ret.value, 256)
            print("Some error occurred. And the last error code is %#x. [%s]" % (ret.value, enumStr))
        return data[0]
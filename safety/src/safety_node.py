#!/usr/bin/env python3.8

import rospy
import time
# from safety.msg import emc
from usbdio.msg import io_in
from usbdio.msg import io_out
from idas_driver.msg import idas
from std_msgs.msg import UInt8
from threading import Thread

thread = Thread()

class Safety():
  def __init__(self):
    print("safety node start")
    rospy.init_node('safety', anonymous=True)
    rospy.Subscriber("/io_in", io_in, self.physical_emergency)
    rospy.Subscriber("/idas_driver", idas, self.idas_emergency)
    rospy.Subscriber("/emc_stop", UInt8, self.emc_callback)
    rospy.Subscriber("/emc_speed", UInt8, self.emcSpeed_callback)
    self.pub_io= rospy.Publisher("/io_out", io_out, queue_size=1)
    self.pub_emc = rospy.Publisher("/emc_stop", UInt8, queue_size=1)
    self.pub_emc = rospy.Publisher("/emc_speed", UInt8, queue_size=1)

    # variable for emergency
    self.emc_btn = 0
    self.emc_idas = 0
    #self.emergency_msg = 0
    self.emc = 0
    self.emc_speed = 0

    self.rate = rospy.Rate(10)
    thread(target=self.main())
    # rospy.spin()

  # when physical_emergency button and bumper is pushed
  def physical_emergency(self, data):
    if data.emc_btn == 254:
      self.emc_btn = 0
    else:
      self.emc_btn = 0

  # idas joystick emergency signal
  def idas_emergency(self, data):
    if data.KY1 == 128:
      self.emc_idas = 1
    else:
      self.emc_idas = 0

  def emc_callback(self, data):
    self.emc = data.data

  def emcSpeed_callback(self, data):
    self.emc_speed = data.data

  # when emergency occur
  def emergency(self):
    out_message = io_out()
    emc_msgs = UInt8()

    while not rospy.is_shutdown():
      # when emergency on
      if self.emc_btn or self.emc_idas or self.emc:
        out_message.redL = 1
        out_message.greenL = 0
        out_message.orangeL = 0
        out_message.buzzer = 1
        out_message.convayor = 0
        out_message.pw_relay = 0
        #emc_msgs.data = 1

      elif self.emc_speed == 1:
          out_message.redL = 0
          out_message.greenL = 0
          out_message.orangeL = 1
          out_message.buzzer = 1
          out_message.convayor = 0
          out_message.pw_relay = 1
          time.sleep(1)
          out_message.orangeL = 0
          out_message.buzzer = 0
          time.sleep(1)
          #emc_msgs.data = 1

      #when emergency off
      else:
        out_message.redL = 0
        out_message.greenL = 0
        out_message.orangeL = 1
        out_message.buzzer = 0
        out_message.convayor = 0
        out_message.pw_relay = 1
        #emc_msgs.data = 0

      self.pub_io.publish(out_message)
      #self.pub_emc.publish(emc_msgs)
      self.rate.sleep()

  def main(self):
    self.emergency()

if __name__ == "__main__":
    Safety()